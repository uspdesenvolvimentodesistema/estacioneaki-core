package br.com.each.usp.testes.validadores;

import static org.junit.Assert.assertTrue;

import javax.faces.validator.ValidatorException;

import org.junit.Test;

import br.com.each.usp.validator.ValidadorCPF;

public class ValidadorCPFTest {

private ValidadorCPF validadorCpf = new ValidadorCPF();
	
	@Test(expected=ValidatorException.class)
	public void testCpfVazio() {
		validadorCpf.validate("");
	}
	
	@Test(expected=ValidatorException.class)
	public void testCpfInvalido() {
		validadorCpf.validate("12312312312");
	}
	
	@Test(expected=ValidatorException.class)
	public void testCpfSequencial() {
		validadorCpf.validate("11111111111");
	}
	
	@Test(expected=ValidatorException.class)
	public void testCpfquantidadeMenor() {
		validadorCpf.validate("1111133");
	}

	
	@Test(expected=ValidatorException.class)
	public void testCpfquantidadeMaior() {
		validadorCpf.validate("11111339837498379");
	}
	
	@Test
	public void testCpfcorreto() {
		// cpf retirado do site http://www.geradordecpf.org/
		validadorCpf.validate("54099547264");
		assertTrue(Boolean.TRUE);
	}

}
