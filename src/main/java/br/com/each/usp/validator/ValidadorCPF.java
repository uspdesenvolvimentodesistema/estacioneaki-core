package br.com.each.usp.validator;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;


public class ValidadorCPF{

	
	public void validate(Object value)
			throws ValidatorException {

		if (value == null) {
       }
		
       String cpf = (String) value;
       if(cpf.length() !=11){
    	   FacesMessage mensagem = new FacesMessage();
    	   mensagem.setDetail("Informe apenas números");
			mensagem.setSummary("CPF Inválido, informe apenas números");
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(mensagem);   
       }
       else if (!calcularDigitoVerificador(cpf.substring(0, 9)).equals(
                                       cpf.substring(9, 11))) {
    	   FacesMessage mensagem = new FacesMessage();
			mensagem.setDetail("Favor informar um CPF Válido");
			mensagem.setSummary("CPF Inválido, Favor informar um CPF Válido");
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(mensagem);   
       }
}

private String calcularDigitoVerificador(String num) throws ValidatorException{
       Integer primDig, segDig;
       int soma = 0, peso = 10;
       for (int i = 0; i < num.length(); i++) {
    	   try {
    		   soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
		} catch (Exception e) {
			 FacesMessage mensagem = new FacesMessage();
				mensagem.setDetail("Informe apenas números");
				mensagem.setSummary("CPF Inválido, informe apenas números");
				mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(mensagem);  
		}
               
       }
       if(sequencial(num)){
    		 FacesMessage mensagem = new FacesMessage();
				mensagem.setDetail("Informe apenas números");
				mensagem.setSummary("CPF Inválido, informe um CPF válido");
				mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(mensagem);  
		   
       }
       if (soma % 11 == 0 | soma % 11 == 1) {
               primDig = new Integer(0);
       } else {
               primDig = new Integer(11 - (soma % 11));
       }
       soma = 0;
       peso = 11;
       for (int i = 0; i < num.length(); i++) {
               soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
       }
       soma += primDig.intValue() * 2;
       if (soma % 11 == 0 | soma % 11 == 1) {
               segDig = new Integer(0);
       } else {
               segDig = new Integer(11 - (soma % 11));
       }
       return primDig.toString() + segDig.toString();
}

private boolean sequencial(String num) {
	for(int i=0; i<10;i++){
		if(num.replace(String.valueOf(i), "").length()==0){
			return true;
		}
	}
	
	return false;
}
		
	

}
