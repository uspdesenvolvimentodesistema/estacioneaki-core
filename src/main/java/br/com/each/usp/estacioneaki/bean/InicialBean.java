package br.com.each.usp.estacioneaki.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class InicialBean implements Serializable {
	 
		private static final long serialVersionUID = 1L;

	public String getCadastro(){
		return "cadastrocliente";
	}
}
