package br.com.each.usp.estacioneaki.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.xml.bind.ValidationException;

import br.com.each.usp.validator.ValidadorCPF;
import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Veiculo;
import br.com.usp.each.estacioaki.domain.dao.ClienteDAO;
import br.com.usp.each.estacioaki.domain.dao.JpaUtil;
import br.com.usp.each.estacioaki.domain.dao.VeiculoDAO;

@ManagedBean
@SessionScoped
public class CadastroClienteBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String msg;
	private Cliente cliente;
	private Veiculo veiculo;
	public CadastroClienteBean(){
		cliente = new Cliente();
		veiculo = new Veiculo();
		msg = new String();
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	  public void save(ActionEvent actionEvent) throws ValidationException{  
	      
		  
		  cliente.getVeiculos().add(veiculo);
	      EntityManager entityManager = null;
	      new JpaUtil();
	      entityManager = JpaUtil.startTransaction(entityManager);
	      
	      if(!new ClienteDAO().jaExiste(cliente.getCPF_CNPJ(), entityManager)){
	    	  new VeiculoDAO().save(veiculo, entityManager);
	      	  new ClienteDAO().save(cliente, entityManager);
	      	  setMsg("");
	      	  JpaUtil.finishTransaction(entityManager);
			  try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("cadastroClienteSucesso.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			 	cliente = new Cliente();
		     	veiculo = new Veiculo();
	      }
	      else{
	    	  setMsg("CPF j� cadastrado por outro usu�rio");
	      }
	    
	  }  
	  
	  public void cancelar(ActionEvent actionEvent) {  
		  	cliente = new Cliente();
	     	veiculo = new Veiculo();
		  try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
	        
	  }  

	  public String getMsg() {
		return msg;
	}
	  
	  public void setMsg(String msg) {
		this.msg = msg;
	}
	  
	public void validadorCPF(FacesContext contexto, UIComponent validar, Object valor)	
				throws ValidatorException {

		ValidadorCPF cpf = new ValidadorCPF();
		cpf.validate(valor);	
	}
	
}
